'''
@author: Klaus
'''
from template import Template
from barcode import Barcode

if __name__ == '__main__':
    b = Barcode(662)
    b.set_barcode_image(True)
    b.set_number_label()
    b.draw_label_on_barcode()
    b.save_to_file()
    
#     template = Template()
#     template.initialize_template()
#     template.draw_borders_on_template(True)
#     for barcode_number, position_number in [(99999999, 27)]:
#         barcode = Barcode(barcode_number)
#         barcode.set_barcode_image(True)
#         barcode.set_number_label()
#         barcode.draw_label_on_barcode()
#         template.paint_barcode_on_template(barcode, position_number)
#     template.save_template_image()
    
    print "done"
