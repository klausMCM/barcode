'''
@author: Klaus
'''

import os
import subprocess
import StringIO
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


class Barcode:

    DPI = 600

    def __init__(self, barcode_number, barcode_type=None, data_prefix=None):
        '''
        Constructor

        :param barcode_number:
            Unique number of a barcode.
        :param barcode_type:
            Default is currently "dmtx".
        :param data_prefix:
            Default is currently "6a49b879" (do not need to change until
            barcode 1-99,999,999 have been used up - 100,000,000 changes the
            dimensions of the barcodes)
        '''
        self.data_prefix = data_prefix or "6a49b879"
        self.barcode_type = barcode_type or "dmtx"
        self.barcode_number = barcode_number
        self.module_size = 20
        self.number_label_image = Image
        self.barcode_image = Image

    def get_dimensions(self):
        return self.barcode_image.size

    def get_barcode_image(self):
        return self.barcode_image

    def get_barcode_number(self):
        return self.barcode_number

    def set_barcode_image(self, with_border=None):
        '''
        Generate barcode_image using dmtxwrite.
        Barcodes margin (quiet zone) is twice the module size.
        with_border determines whether to draw a border around the barcode
        (adds a one pixel black line on each side).

        :param with_border:
            True if barcode should be generated with a 1 pixel border.
        '''
        with_border = with_border or False
        barcode_data = self.data_prefix + "_" + str(self.barcode_number)
        arg1 = "--module=" + str(self.module_size)
        arg2 = "--margin=" + str(self.module_size * 2)
        arg3 = "--resolution=" + str(self.DPI)
        arg4 = "--symbol-size=s"
        #======================================================================
        # TODO:    tricky things going on here - study it in detail
        # https://stackoverflow.com/questions/15975714/create-image-object-from-image-stdout-output-of-external-program-in-python
        # p1 = subprocess.Popen(["echo", barcode_data], stdout=subprocess.PIPE)
        # p2 = subprocess.Popen(["dmtxwrite", arg1, arg2, arg3, arg4],
        # stdin=p1.stdout, stdout=subprocess.PIPE)
        # raw = p2.stdout.read()
        # buff = StringIO.StringIO()
        # buff.write(raw)
        # buff.seek(0)
        # self.barcode_image = Image.open(buff)
        #======================================================================
        p1 = subprocess.Popen(["echo", barcode_data], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["dmtxwrite", arg1, arg2, arg3, arg4],
                              stdin=p1.stdout, stdout=subprocess.PIPE)
        raw = p2.stdout.read()
        buff = StringIO.StringIO()
        buff.write(raw)
        buff.seek(0)
        self.barcode_image = Image.open(buff)
        self.barcode_image = self.barcode_image.convert("RGB")
        if with_border:
            border = Image.new("RGB", (402, 402), "black")
            border.paste(self.barcode_image, (1, 1))
            self.barcode_image = border

    def set_number_label(self):
        '''
        Generate label for barcode which is the barcode's number in gray
        rotated 45 degrees.
        '''
        src_dir = os.getcwd()
        os.chdir('..')

        label_font = ImageFont.truetype((os.path.join(os.getcwd(), "files",
                                                      "DejaVuSansMono.ttf")),
                                        66)
        color = "#787878"  # gray
        number_label_width, number_label_height = \
            label_font.getsize(str(self.barcode_number))
        label = Image.new("RGBA",
                          (number_label_width, number_label_height),
                          (0, 0, 0, 0))
        label_draw = ImageDraw.Draw(label)
        label_draw.text((0, 0), str(self.barcode_number), font=label_font,
                        fill=color)
        label = label.rotate(45, expand=1)
        self.number_label_image = label

        os.chdir(src_dir)

    def draw_label_on_barcode(self):
        '''
        Place label in the middle of the barcode.
        '''
        barcode_width, barcode_height = self.barcode_image.size
        number_label_width, number_label_height = self.number_label_image.size
        x_coord = barcode_width / 2 - number_label_width / 2
        y_coord = barcode_height / 2 - number_label_height / 2
        self.barcode_image.paste(self.number_label_image, (x_coord, y_coord),
                                 self.number_label_image)

    def save_to_file(self):
        '''
        Save barcode to a png file.
        '''
        src_dir = os.getcwd()
        os.chdir('..')
        out_dir = os.path.join(os.getcwd(), "files", "output", "DMTX_")
        self.barcode_image.save(out_dir + str(self.barcode_number) + ".png",
                                "PNG",
                                dpi=(self.DPI, self.DPI))
        os.chdir(src_dir)
