'''
@author: Klaus
'''

import os
import math
from PIL import Image
from PIL import ImageDraw


class Template:

    #=========================================================================
    # conversion of 600 dpi to dots/mm (ie. pixels/mm)
    # 600 dpi used because of printer's capability
    #=========================================================================
    DPI = 600
    DPMM = DPI / 25.4

    # TODO    want to be able to place a single barcode onto
    #            the template (by specifying the barcodes data and
    #            position on the template)
    #            ex.
    #                template.place_single_barcode(Barcode(124), 3)
    #                -> placing barcode 124 at position 3

    def __init__(self, paper_type=None, tape_width=None):
        '''
        Constructor
        paper_type is currently set to "default" representing the currently
        only available measurements.
        tape_width (mm) is reflecting current available tape.
        
        :param paper_type:
            Currently only "default" creates a usable template. No other
            paper types are set up.
        :param tape_width:
            Given in mm.
        '''
        self.template_paper_type = paper_type or "default"
        self.tape_width = tape_width or (19 * self.DPMM)
        self.template_image = Image
        self.template_width = 0
        self.template_height = 0
        self.margin_left = 0
        self.margin_right = 0
        self.margin_top = 0
        self.margin_bottom = 0
        self.max_barcodes = 0

    def draw_borders_on_template(self, margins_only=None):
        '''
        Generate and set the template image. This takes into consideration tape
        width, printing margins and size of the sticker paper.
        All measurements are given in or converted to pixels.
        
        :param margins_only:
            True if only margins should be drawn.
        '''
        margins_only = margins_only or False
        tape_width = int(round(self.tape_width * 1))
        barcode_width = 400
        barcode_height = 400
        position_horizontal_line_maker = self.template_margin_top
        position_vertical_line_maker = self.template_margin_left

        template_draw = ImageDraw.Draw(self.template_image)
        #======================================================================
        # drawing the margin lines
        #======================================================================
        template_draw.line([self.template_margin_left,
                            self.template_margin_top,
                            self.template_margin_left,
                            self.template_height - self.template_margin_bottom],
                           fill="black", width=1)
        template_draw.line([self.template_width - self.template_margin_right,
                            self.template_margin_top,
                            self.template_width - self.template_margin_right,
                            self.template_height - self.template_margin_bottom],
                           fill="black", width=1)
        template_draw.line([self.template_margin_left,
                            self.template_margin_top,
                            self.template_width - self.template_margin_right,
                            self.template_margin_top],
                           fill="black", width=1)
        template_draw.line([self.template_margin_right,
                            self.template_height - self.template_margin_bottom,
                            self.template_width - self.template_margin_right,
                            self.template_height - self.template_margin_bottom],
                           fill="black", width=1)
        if not margins_only:
            #==================================================================
            # drawing the vertical lines
            #==================================================================
            while (position_vertical_line_maker + 1 + barcode_width <
                   self.template_width - self.template_margin_right):
                position_vertical_line_maker += 1
                position_vertical_line_maker += barcode_width
                template_draw.line([position_vertical_line_maker,
                                    self.template_margin_top,
                                    position_vertical_line_maker,
                                    self.template_height -
                                    self.template_margin_bottom],
                                   fill="black", width=1)

                position_vertical_line_maker -= barcode_width
                position_vertical_line_maker += tape_width
                if (position_vertical_line_maker <
                        self.template_width - self.template_margin_right):
                    template_draw.line([position_vertical_line_maker,
                                        self.template_margin_top,
                                        position_vertical_line_maker,
                                        self.template_height - self.template_margin_bottom],
                                       fill="black", width=1)
            #==================================================================
            # drawing the horizontal lines
            #==================================================================
            while (position_horizontal_line_maker + 1 + barcode_height <
                   self.template_height - self.template_margin_bottom):
                position_horizontal_line_maker += 1
                position_horizontal_line_maker += barcode_height
                template_draw.line([self.template_margin_left,
                                    position_horizontal_line_maker,
                                    self.template_width -
                                    self.template_margin_right,
                                    position_horizontal_line_maker],
                                   fill="black", width=1)

    def get_template_margins(self):
        '''
        Return tupple containing the template's margins.
        Margins are returned in this order:
        1. top
        2. bottom
        3. left
        4. right
        '''
        return (self.margin_top, self.margin_bottom,
                self.margin_left, self.margin_right)

    def get_template_dimensions(self):
        '''
        Return template's width and height (in that order).
        '''
        return self.template_image.size

    def initialize_template(self):
        '''
        Set multiple attributes of the template object depending on the
        given paper_type.
        Measurements are given in mm and converted to pixels.
        '''
        if self.template_paper_type == "default":
            self.template_width = int(round(104 * self.DPMM))
            self.template_height = int(round(156 * self.DPMM))
            self.template_margin_top = int(round(5 * self.DPMM))
            self.template_margin_bottom = int(round(5 * self.DPMM))
            self.template_margin_left = int(round(5 * self.DPMM))
            self.template_margin_right = int(round(5 * self.DPMM))
            self.template_image = Image.new("RGB", (self.template_width,
                                                    self.template_height),
                                            "white")
            self.max_barcodes = 40

    def save_template_image(self):
        '''
        Write template image to an image file.
        '''
        src_dir = os.getcwd()
        os.chdir('..')
        self.template_image.save(os.path.join(os.getcwd(), "files", "output",
                                              "product.png"), "PNG",
                                 dpi=(self.DPI, self.DPI))
        os.chdir(src_dir)

    def paint_barcode_on_template(self, barcode, position):
        '''
        Currently, position is one of

                column
                1  2  3  4  5

        row 1   1  2  3  4  5
            2   6  7  8  9 10
            3  11 12 13 14 15
            4  16 17 18 19 20
            5  21 22 23 24 25
            6  26 27 28 29 30
            7  31 32 33 34 35
            8  36 37 38 39 40
        
        :param barcode:
            Barcode to be pasted.
        :param position:
            See diagram above for valid entries.
        '''
        # TODO:    still need to take into consideration width of tape
        column = ((position - 1) % 5 + 1)
        row = int(math.ceil(position / float(5)))
        
        barcode_coordinates = (450 * column - 331, 401 * row - 282)
        self.template_image.paste(
            barcode.get_barcode_image(), barcode_coordinates)
